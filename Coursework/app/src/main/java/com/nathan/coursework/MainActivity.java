package com.nathan.coursework;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Properties;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    final private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    final CharSequence settings[] = {"Road Map","Satellite","Hybrid","Terrain"};
    LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File file = new File("MAP_TYPE.txt");
        if(file.exists())
        {

        }
        else
        {
            writeProperties("ROAD_MAP");
        }
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getPermissions();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                latLng = place.getLatLng();
                changeActivity2();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }

    }

    public void getLocation(View view){
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("", "An error occurred: " + status);
            }
        });
        int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            Log.i("", "An error occurred: ");
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.i("", "An error occurred: ");
        }
    }

    //Show the about app dialog
    public void showAbout(View view) {
        //Show's alert
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.launcher);
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.app_descrip);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create();
        builder.show();
    }

    //show the settings dialog
    public void showSettings(View view) throws IOException {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_setting_dark);
        builder.setTitle("Settings");
        builder.setSingleChoiceItems(settings, -1, new DialogInterface.OnClickListener() {
            //write the chosen map type to properties file
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                if (settings[arg1] == "Road Map") {
                    writeProperties("ROAD_MAP");
                }
                if (settings[arg1] == "Satellite") {
                    writeProperties("SATELLITE");
                }
                if (settings[arg1] == "Hybrid") {
                    writeProperties("HYBRID");
                }
                if (settings[arg1] == "Terrain") {
                    writeProperties("TERRAIN");
                }
            }
        });
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }});
        builder.create();
        builder.show();
    }

    public void writeProperties(String mapType){
        try {
            FileOutputStream fos = openFileOutput("MAP_TYPE.txt", Context.MODE_PRIVATE);
            fos.write(mapType.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void changeActivity(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            new AlertDialog.Builder(this)
                    .setTitle("Requires Permission")
                    .setMessage("This app requires permission to access your location. To continue please press \"Allow\".")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getPermissions();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        else {
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
        }
    }
    public void changeActivity2() {

            Intent intent = new Intent(this, MapsActivity2.class);
            intent.putExtra("latLng",latLng);
            startActivity(intent);

    }

    public void getPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }
}


