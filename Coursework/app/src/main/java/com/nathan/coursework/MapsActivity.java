package com.nathan.coursework;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "MapsActivity";
    private GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    SupportMapFragment mapFragment;
    LatLng latLng;
    GoogleMap mGoogleMap;
    Marker currLocationMarker;
    final private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    private static final String PLACES_SEARCH_URL =  "https://maps.googleapis.com/maps/api/place/search";
    private static final boolean PRINT_AS_STRING = false;
    private int userIcon, drinkIcon;
    private Marker[] placeMarkers;
    private final int MAX_PLACES=3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            getPermissions();
        }
        placeMarkers = new Marker[MAX_PLACES];
        drinkIcon = R.drawable.blue_point;
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    public void getMapType() {
        /*
        File file = new File("MAP_TYPE.txt");
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String mapType;
            while ((mapType = br.readLine()) != null) {

                text.append(mapType);
                text.append('\n');
            }
            if (mapType == "SATELLITE") {
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return;
            }

            if (mapType == "HYBRID") {
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return;
            }

            if (mapType == "TERRAIN") {
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return;
            }
            */
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
/*
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

    }

    //builds the google api client so the app can access the user's location
    //amd surrounding places
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    //Android 6.0 introduced new permission requests which this handles
    public void getPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        // MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION is an
        // app-defined int constant. The callback method gets the
        // result of the request.

    }

    //Manipulates the map once availables
    @Override
    public void onMapReady(GoogleMap gMap) {
        mGoogleMap = gMap;
        getMapType();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            getPermissions();
        }
        mGoogleMap.setMyLocationEnabled(true);
        buildGoogleApiClient();

        //connects the the api client now that it's built
        mGoogleApiClient.connect();

    }


    //when the api is connected gets the nearby places
    @Override
    public void onConnected(Bundle bundle) {
        updatePlaces();

    }

    private void updatePlaces(){

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            getPermissions();
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        double lat = mLastLocation.getLatitude();
        double lng = mLastLocation.getLongitude();

        latLng = new LatLng(lat,lng);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        //Connection string to get the nearby places JSON file
        String placesSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/" +
                "json?location="+lat+","+lng+
                "&rankby=distance"+
                "&types=bar"+
                "&key=AIzaSyBmByY9CRq8JsIRd381c2hZnyGdpDFMTks"+
                "&sensor=true";
        new GetPlaces().execute(placesSearchStr);
    }
    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {

        //zoom to current position:
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(14).build();

        mGoogleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }



//class is processed asynchronously to the rest of the application so as not to interfere with user interaction
    private class GetPlaces extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... placesURL) {

            StringBuilder placesBuilder = new StringBuilder();
            //process search parameter string(s)
            for (String placeSearchURL : placesURL) {
                HttpClient placesClient = new DefaultHttpClient();
                try {
                    HttpGet placesGet = new HttpGet(placeSearchURL);
                    HttpResponse placesResponse = placesClient.execute(placesGet);
                    StatusLine placesSearchStatus = placesResponse.getStatusLine();
                    if (placesSearchStatus.getStatusCode() == 200) {
                        HttpEntity placesEntity = placesResponse.getEntity();
                        InputStream placesContent = placesEntity.getContent();
                        InputStreamReader placesInput = new InputStreamReader(placesContent);
                        BufferedReader placesReader = new BufferedReader(placesInput);
                        String lineIn;
                        while ((lineIn = placesReader.readLine())!=null)
                        {
                           placesBuilder.append(lineIn);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return placesBuilder.toString();
        }
/*
    protected String getRating(String placeId) {
        StringBuilder ratingBuilder = new StringBuilder();
        String url = "https://maps.googleapis.com/maps/api/place/details/json"+
                "?placeid="+placeId+
                "&key=AIzaSyBmByY9CRq8JsIRd381c2hZnyGdpDFMTks";

        HttpClient placesClient = new DefaultHttpClient();
        try {
            HttpGet placesGet = new HttpGet(url);
            HttpResponse placesResponse = placesClient.execute(placesGet);
            StatusLine placesSearchStatus = placesResponse.getStatusLine();
            if (placesSearchStatus.getStatusCode() == 200) {
                HttpEntity placesEntity = placesResponse.getEntity();
                InputStream placesContent = placesEntity.getContent();
                InputStreamReader placesInput = new InputStreamReader(placesContent);
                BufferedReader placesReader = new BufferedReader(placesInput);
                String lineIn;
                while ((lineIn = placesReader.readLine()) != null) {
                    ratingBuilder.append(lineIn);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return ratingBuilder.toString();
    }
*/

        //Calculates the distance between the place and the user
        private String distance(double lat1, double lon1, double lat2, double lon2) {
            //calculate distance in miles
            double theta = lon1 - lon2;
            double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            int time;
            String distanceTime;
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.2;

            //Round time to nearest 5
            time = (int) (dist/2.5*60);
            if(time>0 && time <7)
            {
                time = 5;
            }
            if(time>6 && time <12)
            {
                time = 10;
            }
            if(time>11 && time <17)
            {
                time = 15;
            }
            if(time>16 && time <22)
            {
                time = 20;
            }
            if(time>21 && time <27)
            {
                time = 25;
            }


            //round dist to 2 dp to be more presentable
            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);
            dist = Double.valueOf(df.format(dist));


            //create return string
            distanceTime = String.valueOf(dist)+" miles | approx. "+String.valueOf(time)+" minute walk";
            return (distanceTime);
        }

        private double deg2rad(double deg) {
            return (deg * Math.PI / 180.0);
        }

        private double rad2deg(double rad) {
            return (rad * 180 / Math.PI);
        }

        private MarkerOptions[] places;
        protected void onPostExecute(String result) {
            if (placeMarkers!=null){
                for(int pm=0; pm<placeMarkers.length; pm++){
                    if(placeMarkers[pm]!=null){
                        placeMarkers[pm].remove();
                    }
                    try {
                        JSONObject resultObject = new JSONObject(result);
                        JSONArray placesArray = resultObject.getJSONArray("results");
                        places = new MarkerOptions[placesArray.length()];
                        for (int p=0; p<placesArray.length(); p++) {
                            boolean missingValue=false;
                            LatLng placeLL=null;
                            LatLng userLL=null;
                            String placeName="";
                            String vicinity="";
                            String placeId;
                            int currIcon = drinkIcon;
                            try{
                                missingValue=false;
                                JSONObject placeObject = placesArray.getJSONObject(p);
                                JSONObject loc = placeObject.getJSONObject("geometry").getJSONObject("location");
                                placeLL = new LatLng(
                                        Double.valueOf(loc.getString("lat")),
                                        Double.valueOf(loc.getString("lng")));
                                userLL = new LatLng(
                                        Double.valueOf(latLng.latitude),
                                        Double.valueOf(latLng.longitude));
                                vicinity = distance(placeLL.latitude,placeLL.longitude,userLL.latitude,userLL.longitude);
                                //placeId = placeObject.getString("place_id");
                                //String ratingResult = getRating(placeId);
                                //JSONObject ratingObject = new JSONObject(ratingResult);
                                //String rating = ratingObject.getString("rating");
                                placeName = placeObject.getString("name");
                                String title = placeName; //+ " | " + rating +"/5";
                                if(missingValue)
                                {
                                    places[p]=null;
                                }
                                else
                                {
                                    places[p]=new MarkerOptions()
                                            .position(placeLL)
                                            .title(title)
                                            .icon(BitmapDescriptorFactory.defaultMarker())
                                            .snippet(vicinity);
                                }
                            }
                            catch(JSONException jse){
                                missingValue=true;
                                jse.printStackTrace();
                            }
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(places!=null && placeMarkers!=null){
                        for(int p=0; p<places.length && p<placeMarkers.length; p++){
                            if(places[p]!=null)
                            {
                                placeMarkers[p]=mGoogleMap.addMarker(places[p]);
                            }
                        }
                    }

                }
            }
        }
    }


}



